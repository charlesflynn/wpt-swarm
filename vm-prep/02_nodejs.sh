sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
sudo echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" > /etc/apt/sources.list.d/webupd8team-java.list
sudo apt-get update
sudo echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
sudo echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
sudo apt-get install -y oracle-java7-installer python-pip nodejs
